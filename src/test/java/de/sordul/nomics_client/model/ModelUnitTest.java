package de.sordul.nomics_client.model;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import static org.junit.jupiter.api.Assertions.assertNotNull;

public class ModelUnitTest {

	@Test
	public void testExchangeRate() {
		var exchangeRate = new ExchangeRate();
		exchangeRate.setCurrency("BTC");
		exchangeRate.setRate(7223.94125);
		exchangeRate.setTimestamp(Instant.parse("2019-12-13T00:00:00Z"));

		assertNotNull(exchangeRate);
		assertThat(exchangeRate.getRate(), is(7223.94125));
		assertNotNull(exchangeRate.toString());
	}

	@Test
	public void testExchangeRateInterval() {
		var exchangeRateInterval = new ExchangeRateInterval();
		exchangeRateInterval.setCurrency("BTC");
		exchangeRateInterval.setOpen(7223.94125);
		exchangeRateInterval.setOpenTimestamp(Instant.parse("2019-12-13T00:00:00Z"));
		exchangeRateInterval.setClose(7423.94126);
		exchangeRateInterval.setCloseTimestamp(Instant.parse("2019-12-14T00:00:00Z"));


		assertNotNull(exchangeRateInterval);
		assertThat(exchangeRateInterval.getClose(), is(7423.94126));
		assertNotNull(exchangeRateInterval.toString());
	}

	@Test
	public void testIntervalData() {
		var intervalData = new IntervalData();
		intervalData.setPriceChange(269.75208019);
		intervalData.setPriceChangePct(0.03297053);
		intervalData.setVolume(1110989572.04);
		intervalData.setVolumeChange(-24130098.49);
		intervalData.setVolumeChangePct(-0.02);
		intervalData.setMarketCapChange(4805518049.63);
		intervalData.setMarketCapChangePct(0.03);

		assertNotNull(intervalData);
		assertThat(intervalData.getVolumeChange(), is(-24130098.49));
		assertNotNull(intervalData.toString());
	}

	@Test
	public void testMarketCapHistory() {
		var marketCapHistory = new MarketCapHistory();
		marketCapHistory.setTimestamp(Instant.parse("2019-12-13T00:00:00Z"));
		marketCapHistory.setMarketCap(246648894886l);

		assertNotNull(marketCapHistory);
		assertThat(marketCapHistory.getMarketCap(), is(246648894886l));
		assertNotNull(marketCapHistory.toString());
	}

	@Test
	public void testMarketData() {
		var marketData = new MarketData();
		marketData.setExchange("binance");
		marketData.setMarket("BNBTUSD");
		marketData.setBase("BNB");
		marketData.setQuote("TUSD");

		assertNotNull(marketData);
		assertThat(marketData.getBase(), is("BNB"));
		assertNotNull(marketData.toString());
	}

	@Test
	public void testMetaData() {
		var metaData = new MetaData();
		metaData.setId("XEM");
		metaData.setOriginalSymbol("XEM");
		metaData.setName("XEM");
		metaData.setDescription("NEM is a dual-layer blockchain similar to Ethereum but written yada yada");
		metaData.setWebsiteUrl("https://nem.io/");
		metaData.setLogoUrl("https://s3.us-east-2.amazonaws.com/nomics-api/static/images/currencies/xem.svg");
		metaData.setRedditUrl("https://www.reddit.com/r/nem/");
		metaData.setWhitepaperUrl("https://docs.nem.io/en");

		assertNotNull(metaData);
		assertThat(metaData.getWebsiteUrl(), is("https://nem.io/"));
		assertNotNull(metaData.toString());
	}

	@Test
	public void testSparklineEntry() {
		var sparklineEntry = new SparklineEntry();
		sparklineEntry.setCurrency("OVC");
		
		var timestamps = new ArrayList<LocalDate>();
		timestamps.add(LocalDate.ofInstant(Instant.parse("2019-10-28T00:00:00Z"), ZoneId.systemDefault()));
		timestamps.add(LocalDate.ofInstant(Instant.parse("2019-11-04T00:00:00Z"), ZoneId.systemDefault()));
		sparklineEntry.setTimestamps(timestamps);

		var prices = new ArrayList<Double>();
		prices.add(0.02567079);
		prices.add(0.03057079);
		sparklineEntry.setPrices(prices);

		assertNotNull(sparklineEntry);
		assertThat(sparklineEntry.getPrices().get(0), is(0.02567079));
		assertNotNull(sparklineEntry.toString());
	}

	@Test
	public void testTickerData() {
		var tickerData = new TickerData();
		tickerData.setId("BTC");
		tickerData.setCurrency("BTC");
		tickerData.setSymbol("BTC");
		tickerData.setName("Bitcoin");
		tickerData.setLogoUrl("https://s3.us-east-2.amazonaws.com/nomics-api/static/images/currencies/btc.svg");
		tickerData.setRank(1);
		tickerData.setPrice(7233.14365205);
		tickerData.setPriceDate(Instant.parse("2019-12-13T00:00:00Z"));
		tickerData.setMarketCap(130914294416l);
		tickerData.setCirculatingSupply(18099225l);
		tickerData.setMaxSupply(21000000l);
		tickerData.setHigh(19409.84685355);
		tickerData.setHighTimestamp(Instant.parse("2017-12-16T00:00:00Z"));

		assertNotNull(tickerData);
		assertThat(tickerData.getPrice(), is(7233.14365205));
		assertNotNull(tickerData.toString());
	}

	@Test
	public void testVolume() {
		var volume = new Volume();
		volume.setTimestamp(Instant.parse("2018-04-14T00:00:00Z"));
		volume.setVolume(5209035491l);

		assertNotNull(volume);
		assertThat(volume.getVolume(), is(5209035491l));
		assertNotNull(volume.toString());
	}
}
