package de.sordul.nomics_client.impl;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.LocalDate;
import java.time.Month;
import java.time.temporal.ChronoUnit;
import java.util.Map;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import com.google.common.collect.ImmutableMap;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class MarketsEndpointIntegrationTest {
	private MarketsEndpoint marketsEndpoint;

	@BeforeAll
	public void init() {

		// Get API key either from enviroment variable or configuration
		var apiKey = System.getenv("NOMICS_KEY");
		if (apiKey == null) {
			apiKey = ConfigUtil.getProp("apiKey");
		}

		// Instantiate a CurrenciesEndpoint object with an api key
		marketsEndpoint = new MarketsEndpoint(apiKey);
	}

	@Test
	public void testGetMarketDataWithExchangeAndBaseCurrency() {

		Map<String, Object> requestParams = ImmutableMap.of(
			"exchange", "binance",
			"base", new String[] {"BNB", "USDC"}
		);
		var result = marketsEndpoint.getMarketData(requestParams);
		assertNotNull(result);
		assertTrue(result.size() > 10);
		assertTrue(result.get(0).getExchange().equals("binance"));
	}

	@Test
	public void testGetMarketDataAsCSV() {
		Map<String, Object> requestParams = ImmutableMap.of(
			"exchange", "okex",
			"quote", new String[] {"USDT"}
		);
		var result = marketsEndpoint.getMarketDataAsCSV(requestParams);
		assertNotNull(result);
	}

	@Test
	public void testGetMarketCapHistory() {
		var startDate = LocalDate.of(2019, Month.NOVEMBER, 10);

		// Expecting 2019-11-17 as current date
		Map<String, Object> requestParams = ImmutableMap.of(
			"start", startDate
		);
		var result = marketsEndpoint.getMarketCapHistory(requestParams);
		assertNotNull(result);
		assertThat((long)result.size(), is(ChronoUnit.DAYS.between(startDate, LocalDate.now()) + 1));
	}

	@Test
	public void testGetMarketCapHistoryAsCSV() {

		Map<String, Object> requestParams = ImmutableMap.of(
			"start", LocalDate.of(2019, Month.OCTOBER, 10)
		);
		var result = marketsEndpoint.getMarketCapHistoryAsCSV(requestParams);
		assertNotNull(result);
	}
}