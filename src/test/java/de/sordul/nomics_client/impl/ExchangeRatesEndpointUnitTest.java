package de.sordul.nomics_client.impl;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.Month;
import java.util.LinkedHashMap;
import java.util.Map;

import com.google.common.collect.ImmutableMap;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.internal.util.reflection.FieldSetter;

import de.sordul.nomics_client.IRESTClient;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class ExchangeRatesEndpointUnitTest {
	private ExchangeRatesEndpoint exchangeRatesEndpoint;
	private final String apiKey = "yeahForSureIWillPutARealKeyHere";

	@BeforeAll
	public void init() {
		exchangeRatesEndpoint = new ExchangeRatesEndpoint(apiKey);
	}

	@Test
	public void testGetExchangeRates() throws NoSuchFieldException, SecurityException, IOException {

		// Load prepared JSON response from file
		var json = FileUtils.readFileToString(
			Paths.get("src/test/resources/response_testGetExchangeRates.json").toFile(),
			Charset.forName("UTF-8")
		);

		// Set URL params
		var params = new LinkedHashMap<String, String>();
		params.put("key", this.apiKey);

		// Set up the IRESTClient within our ExchangeRatesEndpoint
		var mockedRestClient = mock(IRESTClient.class);
		when(mockedRestClient.callEndpoint("exchange-rates", params)).thenReturn(json);
		FieldSetter.setField(exchangeRatesEndpoint, exchangeRatesEndpoint.getClass().getDeclaredField("restClient"), mockedRestClient);

		var result = exchangeRatesEndpoint.getExchangeRates();
		assertNotNull(result);
		assertThat(result.size(), is(169));
		assertTrue(result.get(0).getCurrency().equals("AED"));
	}

	@Test
	public void testGetExchangeRatesAsCSV() throws NoSuchFieldException, SecurityException, IOException {

		// Load prepared JSON response from file
		var json = FileUtils.readFileToString(
			Paths.get("src/test/resources/response_testGetExchangeRatesAsCSV.csv").toFile(),
			Charset.forName("UTF-8")
		);

		// Set URL params
		var params = new LinkedHashMap<String, String>();
		params.put("key", this.apiKey);
		params.put("format", "csv");

		// Set up the IRESTClient within our ExchangeRatesEndpoint
		var mockedRestClient = mock(IRESTClient.class);
		when(mockedRestClient.callEndpoint("exchange-rates", params)).thenReturn(json);
		FieldSetter.setField(exchangeRatesEndpoint, exchangeRatesEndpoint.getClass().getDeclaredField("restClient"), mockedRestClient);

		var result = exchangeRatesEndpoint.getExchangeRatesAsCSV();
		assertNotNull(result);
	}

	@Test
	public void testGetExchangeRatesHistory() throws NoSuchFieldException, SecurityException, IOException {

		// Load prepared JSON response from file
		var json = FileUtils.readFileToString(
			Paths.get("src/test/resources/response_testGetExchangeRatesHistory.json").toFile(),
			Charset.forName("UTF-8")
		);

		// Set URL params
		var params = new LinkedHashMap<String, String>();
		params.put("key", this.apiKey);
		params.put("start", "2019-11-15T00:00:00Z");

		// Set up the IRESTClient within our ExchangeRatesEndpoint
		var mockedRestClient = mock(IRESTClient.class);
		when(mockedRestClient.callEndpoint("exchange-rates/history", params)).thenReturn(json);
		FieldSetter.setField(exchangeRatesEndpoint, exchangeRatesEndpoint.getClass().getDeclaredField("restClient"), mockedRestClient);

		// Expecting 2019-11-15 as current date
		Map<String, Object> requestParams = ImmutableMap.of(
			"start", LocalDate.of(2019, Month.NOVEMBER, 15)
		);
		var result = exchangeRatesEndpoint.getExchangeRatesHistory(requestParams);
		assertNotNull(result);
		assertThat(result.size(), is(3));
	}

	@Test
	public void testGetExchangeRatesHistoryAsCSV() throws NoSuchFieldException, SecurityException, IOException {

		// Load prepared JSON response from file
		var json = FileUtils.readFileToString(
			Paths.get("src/test/resources/response_testGetExchangeRatesHistoryAsCSV.csv").toFile(),
			Charset.forName("UTF-8")
		);

		// Set URL params
		var params = new LinkedHashMap<String, String>();
		params.put("key", this.apiKey);
		params.put("start", "2019-11-15T00:00:00Z");
		params.put("format", "csv");

		// Set up the IRESTClient within our ExchangeRatesEndpoint
		var mockedRestClient = mock(IRESTClient.class);
		when(mockedRestClient.callEndpoint("exchange-rates/history", params)).thenReturn(json);
		FieldSetter.setField(exchangeRatesEndpoint, exchangeRatesEndpoint.getClass().getDeclaredField("restClient"), mockedRestClient);

		// Expecting 2019-11-15 as current date
		Map<String, Object> requestParams = ImmutableMap.of(
			"start", LocalDate.of(2019, Month.NOVEMBER, 15)
		);
		var result = exchangeRatesEndpoint.getExchangeRatesHistoryAsCSV(requestParams);
		assertNotNull(result);
	}

	@Test
	public void testGetExchangeRatesInterval() throws NoSuchFieldException, SecurityException, IOException {

		// Load prepared JSON response from file
		var json = FileUtils.readFileToString(
			Paths.get("src/test/resources/response_testGetExchangeRatesInterval.json").toFile(),
			Charset.forName("UTF-8")
		);

		// Set URL params
		var params = new LinkedHashMap<String, String>();
		params.put("key", this.apiKey);
		params.put("start", "2019-11-15T00:00:00Z");

		// Set up the IRESTClient within our ExchangeRatesEndpoint
		var mockedRestClient = mock(IRESTClient.class);
		when(mockedRestClient.callEndpoint("exchange-rates/interval", params)).thenReturn(json);
		FieldSetter.setField(exchangeRatesEndpoint, exchangeRatesEndpoint.getClass().getDeclaredField("restClient"), mockedRestClient);

		// Expecting 2019-11-15 as current date
		Map<String, Object> requestParams = ImmutableMap.of(
			"start", LocalDate.of(2019, Month.NOVEMBER, 15)
		);
		var result = exchangeRatesEndpoint.getExchangeRatesInterval(requestParams);
		assertNotNull(result);
		assertThat(result.size(), is(169));
		assertTrue(result.get(0).getCurrency().equals("AED"));
	}

	@Test
	public void testGetExchangeRatesIntervalAsCSV() throws NoSuchFieldException, SecurityException, IOException {

		// Load prepared JSON response from file
		var json = FileUtils.readFileToString(
			Paths.get("src/test/resources/response_testGetExchangeRatesIntervalAsCSV.csv").toFile(),
			Charset.forName("UTF-8")
		);

		// Set URL params
		var params = new LinkedHashMap<String, String>();
		params.put("key", this.apiKey);
		params.put("start", "2019-11-15T00:00:00Z");
		params.put("format", "csv");

		// Set up the IRESTClient within our ExchangeRatesEndpoint
		var mockedRestClient = mock(IRESTClient.class);
		when(mockedRestClient.callEndpoint("exchange-rates/interval", params)).thenReturn(json);
		FieldSetter.setField(exchangeRatesEndpoint, exchangeRatesEndpoint.getClass().getDeclaredField("restClient"), mockedRestClient);

		// Expecting 2019-11-15 as current date
		Map<String, Object> requestParams = ImmutableMap.of(
			"start", LocalDate.of(2019, Month.NOVEMBER, 15)
		);
		var result = exchangeRatesEndpoint.getExchangeRatesIntervalAsCSV(requestParams);
		assertNotNull(result);
	}
}