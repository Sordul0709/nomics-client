package de.sordul.nomics_client.impl;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.Month;
import java.util.LinkedHashMap;
import java.util.Map;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import com.google.common.collect.ImmutableMap;

import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.internal.util.reflection.FieldSetter;

import de.sordul.nomics_client.IRESTClient;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class MarketsEndpointUnitTest {
	private MarketsEndpoint marketsEndpoint;
	private final String apiKey = "yeahForSureIWillPutARealKeyHere";

	@BeforeAll
	public void init() {
		marketsEndpoint = new MarketsEndpoint(apiKey);
	}

	@Test
	public void testGetMarketDataWithExchangeAndBaseCurrency() throws IOException, NoSuchFieldException, SecurityException {

		// Load prepared JSON response from file
		var json = FileUtils.readFileToString(
			Paths.get("src/test/resources/response_testGetMarketDataWithExchangeAndBaseCurrency.json").toFile(),
			Charset.forName("UTF-8")
		);

		// Set URL params
		var params = new LinkedHashMap<String, String>();
		params.put("key", this.apiKey);
		params.put("exchange", "binance");
		params.put("base", "BNB,USDC");

		// Set up the IRESTClient within our MarketsEndpoint
		var mockedRestClient = mock(IRESTClient.class);
		when(mockedRestClient.callEndpoint("markets", params)).thenReturn(json);
		FieldSetter.setField(marketsEndpoint, marketsEndpoint.getClass().getDeclaredField("restClient"), mockedRestClient);

		Map<String, Object> requestParams = ImmutableMap.of(
			"exchange", "binance",
			"base", new String[] {"BNB", "USDC"}
		);
		var result = marketsEndpoint.getMarketData(requestParams);
		assertNotNull(result);
		assertThat(result.size(), is(14));
		assertTrue(result.get(0).getExchange().equals("binance"));
	}

	@Test
	public void testGetMarketDataAsCSV() throws IOException, NoSuchFieldException, SecurityException {

		// Load prepared JSON response from file
		var json = FileUtils.readFileToString(
			Paths.get("src/test/resources/response_testGetMarketDataAsCSV.csv").toFile(),
			Charset.forName("UTF-8")
		);

		// Set URL params
		var params = new LinkedHashMap<String, String>();
		params.put("key", this.apiKey);
		params.put("exchange", "okex");
		params.put("quote", "USDT");
		params.put("format", "csv");

		// Set up the IRESTClient within our MarketsEndpoint
		var mockedRestClient = mock(IRESTClient.class);
		when(mockedRestClient.callEndpoint("markets", params)).thenReturn(json);
		FieldSetter.setField(marketsEndpoint, marketsEndpoint.getClass().getDeclaredField("restClient"), mockedRestClient);

		Map<String, Object> requestParams = ImmutableMap.of(
			"exchange", "okex",
			"quote", new String[] {"USDT"}
		);
		var result = marketsEndpoint.getMarketDataAsCSV(requestParams);
		assertNotNull(result);
	}

	@Test
	public void testGetMarketCapHistory() throws IOException, NoSuchFieldException, SecurityException {

		// Load prepared JSON response from file
		var json = FileUtils.readFileToString(
			Paths.get("src/test/resources/response_testGetMarketCapHistory.json").toFile(),
			Charset.forName("UTF-8")
		);

		// Set URL params
		var params = new LinkedHashMap<String, String>();
		params.put("key", this.apiKey);
		params.put("start", "2019-11-10T00:00:00Z");

		// Set up the IRESTClient within our MarketsEndpoint
		var mockedRestClient = mock(IRESTClient.class);
		when(mockedRestClient.callEndpoint("market-cap/history", params)).thenReturn(json);
		FieldSetter.setField(marketsEndpoint, marketsEndpoint.getClass().getDeclaredField("restClient"), mockedRestClient);

		// Expecting 2019-11-17 as current date
		Map<String, Object> requestParams = ImmutableMap.of(
			"start", LocalDate.of(2019, Month.NOVEMBER, 10)
		);
		var result = marketsEndpoint.getMarketCapHistory(requestParams);
		assertNotNull(result);
		assertThat(result.size(), is(7));
		assertTrue(result.get(6).getMarketCap() == 232565794766l);
	}

	@Test
	public void testGetMarketCapHistoryAsCSV() throws IOException, NoSuchFieldException, SecurityException {

		// Load prepared JSON response from file
		var json = FileUtils.readFileToString(
			Paths.get("src/test/resources/response_testGetMarketCapHistoryAsCSV.csv").toFile(),
			Charset.forName("UTF-8")
		);

		// Set URL params
		var params = new LinkedHashMap<String, String>();
		params.put("key", this.apiKey);
		params.put("start", "2019-10-10T00:00:00Z");
		params.put("format", "csv");

		// Set up the IRESTClient within our MarketsEndpoint
		var mockedRestClient = mock(IRESTClient.class);
		when(mockedRestClient.callEndpoint("market-cap/history", params)).thenReturn(json);
		FieldSetter.setField(marketsEndpoint, marketsEndpoint.getClass().getDeclaredField("restClient"), mockedRestClient);

		Map<String, Object> requestParams = ImmutableMap.of(
			"start", LocalDate.of(2019, Month.OCTOBER, 10)
		);
		var result = marketsEndpoint.getMarketCapHistoryAsCSV(requestParams);
		assertNotNull(result);
	}
}