package de.sordul.nomics_client.impl;

import java.time.LocalDate;
import java.time.Month;
import java.time.temporal.ChronoUnit;
import java.util.Map;

import com.google.common.collect.ImmutableMap;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class ExchangeRatesEndpointIntegrationTest {
	private ExchangeRatesEndpoint exchangeRatesEndpoint;

	@BeforeAll
	public void init() {

		// Get API key either from enviroment variable or configuration
		var apiKey = System.getenv("NOMICS_KEY");
		if (apiKey == null) {
			apiKey = ConfigUtil.getProp("apiKey");
		}

		// Instantiate a ExchangeRatesEndpoint object with an api key
		exchangeRatesEndpoint = new ExchangeRatesEndpoint(apiKey);
	}

	@Test
	public void testGetExchangeRates() {
		var result = exchangeRatesEndpoint.getExchangeRates();
		assertNotNull(result);
		assertThat(result.size(), is(169));
		assertTrue(result.get(0).getCurrency().equals("AED"));
	}

	@Test
	public void testGetExchangeRatesAsCSV() {
		var result = exchangeRatesEndpoint.getExchangeRatesAsCSV();
		assertNotNull(result);
	}

	@Test
	public void testGetExchangeRatesHistory() {
		var startDate = LocalDate.of(2019, Month.NOVEMBER, 15);
		var daysPassed = ChronoUnit.DAYS.between(startDate, LocalDate.now()) + 1;

		// Expecting 2019-11-15 as current date
		Map<String, Object> requestParams = ImmutableMap.of(
			"start", startDate,
			"currency", "ETH"
		);
		var result = exchangeRatesEndpoint.getExchangeRatesHistory(requestParams);
		assertNotNull(result);
		assertThat((long)result.size(), is(daysPassed));
	}

	@Test
	public void testGetExchangeRatesHistoryAsCSV() {

		// Expecting 2019-11-15 as current date
		Map<String, Object> requestParams = ImmutableMap.of(
			"start", LocalDate.of(2019, Month.NOVEMBER, 15)
		);
		var result = exchangeRatesEndpoint.getExchangeRatesHistoryAsCSV(requestParams);
		assertNotNull(result);
	}

	@Test
	public void testGetExchangeRatesInterval() {

		// Expecting 2019-11-15 as current date
		Map<String, Object> requestParams = ImmutableMap.of(
			"start", LocalDate.of(2019, Month.NOVEMBER, 15)
		);
		var result = exchangeRatesEndpoint.getExchangeRatesInterval(requestParams);
		assertNotNull(result);
		assertThat(result.size(), is(169));
		assertTrue(result.get(0).getCurrency().equals("AED"));
	}

	@Test
	public void testGetExchangeRatesIntervalAsCSV() {

		// Expecting 2019-11-15 as current date
		Map<String, Object> requestParams = ImmutableMap.of(
			"start", LocalDate.of(2019, Month.NOVEMBER, 15)
		);
		var result = exchangeRatesEndpoint.getExchangeRatesIntervalAsCSV(requestParams);
		assertNotNull(result);
	}
}