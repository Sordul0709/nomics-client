package de.sordul.nomics_client.impl;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.Month;
import java.util.LinkedHashMap;
import java.util.Map;

import com.google.common.collect.ImmutableMap;

import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.internal.util.reflection.FieldSetter;

import de.sordul.nomics_client.IRESTClient;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class VolumeEndpointUnitTest {
	private VolumeEndpoint volumeEndpoint;
	private final String apiKey = "yeahForSureIWillPutARealKeyHere";

	@BeforeAll
	void init() {
		volumeEndpoint = new VolumeEndpoint(apiKey);
	}

	@Test
	void testGetVolumeHistory() throws NoSuchFieldException, SecurityException, IOException {

		// Load prepared JSON response from file
		var json = FileUtils.readFileToString(
			Paths.get("src/test/resources/response_testGetVolumeHistory.json").toFile(),
				StandardCharsets.UTF_8
		);

		// Set URL params
		var params = new LinkedHashMap<String, String>();
		params.put("key", this.apiKey);
		params.put("start", "2019-11-15T00:00:00Z");

		// Set up the IRESTClient within our MarketsEndpoint
		var mockedRestClient = mock(IRESTClient.class);
		when(mockedRestClient.callEndpoint("volume/history", params)).thenReturn(json);
		FieldSetter.setField(volumeEndpoint, volumeEndpoint.getClass().getDeclaredField("restClient"), mockedRestClient);

		// Expecting 2019-11-16 as current date
		Map<String, Object> requestParams = ImmutableMap.of(
			"start", LocalDate.of(2019, Month.NOVEMBER, 15)
		);
		var result = volumeEndpoint.getVolumeHistory(requestParams);
		assertNotNull(result);
		assertThat(result.size(), is(44));
		assertEquals(1936709604L, result.get(1).getVolume());
	}
}