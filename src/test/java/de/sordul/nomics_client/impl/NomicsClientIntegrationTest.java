package de.sordul.nomics_client.impl;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.time.LocalDate;
import java.time.Month;
import java.util.Map;

import com.google.common.collect.ImmutableMap;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class NomicsClientIntegrationTest {
	private String apiKey;

	@BeforeAll
	public void init() {

		// Get API key either from enviroment variable or configuration
		apiKey = System.getenv("NOMICS_KEY");
		if (apiKey == null) {
			apiKey = ConfigUtil.getProp("apiKey");
		}
	}

	@Test
	public void testInstances() {
		var client = new NomicsClient(this.apiKey);

		assertNotNull(client.currencies);
		assertNotNull(client.markets);
		assertNotNull(client.volume);
		assertNotNull(client.rates);
	}

	@Test
	public void testBasicFunctionality() {
		var client = new NomicsClient(this.apiKey);
		Map<String, Object> requestParams;

		// Currencies
		requestParams = ImmutableMap.of(
			"currencies", new String[] {"LTC", "TRX", "BTT"}
		);
		var currenciesResult = client.currencies.getTickerData(requestParams);

		// Markets
		requestParams = ImmutableMap.of(
			"exchange", "binance",
			"base", new String[] {"USDT", "BTC"}
		);
		var marketsResult = client.markets.getMarketData(requestParams);

		// Volume
		requestParams = ImmutableMap.of(
			"start", LocalDate.of(2019, Month.JANUARY, 15),
			"end", LocalDate.of(2019, Month.JANUARY, 15)
		);
		var volumeResult = client.volume.getVolumeHistory(requestParams);

		// Rates
		var ratesResult = client.rates.getExchangeRates();

		assertNotNull(currenciesResult);
		assertNotNull(marketsResult);
		assertNotNull(volumeResult);
		assertNotNull(ratesResult);
	}
}