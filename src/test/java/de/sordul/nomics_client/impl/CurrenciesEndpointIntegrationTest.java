package de.sordul.nomics_client.impl;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.LocalDate;
import java.time.Month;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Arrays;
import java.util.stream.Collectors;

import com.google.common.collect.ImmutableMap;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import de.sordul.nomics_client.model.Interval;
import de.sordul.nomics_client.model.MetaAttribute;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class CurrenciesEndpointIntegrationTest {
	private CurrenciesEndpoint currenciesEndpoint;

	@BeforeAll
	public void init() {

		// Get API key either from enviroment variable or configuration
		var apiKey = System.getenv("NOMICS_KEY");
		if (apiKey == null) {
			apiKey = ConfigUtil.getProp("apiKey");
		}

		// Instantiate a CurrenciesEndpoint object with an api key
		currenciesEndpoint = new CurrenciesEndpoint(apiKey);
	}

	@Test
	public void testGetTickerDataWithCurrencies() {

		// Set URL params
		var params = new LinkedHashMap<String, String>();
		params.put("ids", "BTC,ETH,XRP");
		
		Map<String, Object> requestParams = ImmutableMap.of(
			"currencies", new String[] {"BTC", "ETH", "XRP"}
		);
		var result = currenciesEndpoint.getTickerData(requestParams);
		assertNotNull(result);
		var currencyNames = result.stream().map(x -> x.getCurrency()).collect(Collectors.toList());
		assertThat("Should have been 3", result.size(), is(3));
		assertTrue(currencyNames.containsAll(Arrays.asList("BTC", "ETH", "XRP")), "Should contain the queries currencies");
	}

	@Test
	public void testGetTickerDataWithCurrenciesAndInterval() {

		// Set URL params
		var params = new LinkedHashMap<String, String>();
		params.put("ids", "TRX,BTT,XMR");
		params.put("interval", "1d,ytd");

		Map<String, Object> requestParams = ImmutableMap.of(
			"currencies", new String[] {"TRX", "BTT", "XMR"},
			"interval", new Interval[] { Interval.DAY, Interval.CURRENT_YEAR }
		);
		var result = currenciesEndpoint.getTickerData(requestParams);
		assertNotNull(result);
		var currencyNames = result.stream().map(x -> x.getCurrency()).collect(Collectors.toList());
		assertThat("Should have been 3", result.size(), is(3));
		assertTrue(currencyNames.containsAll(Arrays.asList("TRX", "BTT", "XMR")), "Should contain the queries currencies");
	}

	@Test
	public void testGetTickerDataWithCurrenciesAndIntervalAndConvert() {

		// Set URL params
		var params = new LinkedHashMap<String, String>();
		params.put("ids", "BTC");
		params.put("interval", "1d,ytd");
		params.put("convert", "BTC");

		Map<String, Object> requestParams = ImmutableMap.of(
			"currencies", new String[] {"BTC"},
			"interval", new Interval[] { Interval.DAY, Interval.CURRENT_YEAR },
			"convert", "btc"
		);
		var result = currenciesEndpoint.getTickerData(requestParams);
		assertNotNull(result);
		assertThat("Should have been 1", result.size(), is(1));
		assertThat("Should have been 'BTC'", result.get(0).getCurrency(), is("BTC"));
		assertNotNull(result.get(0).getIntervalData());
		assertThat("Should have been '2'", result.get(0).getIntervalData().size(), is(2));
		assertThat("Should have been '1.0'", result.get(0).getPrice(), is(1.0d));
	}

	@Test
	public void testGetMetaDataWithCurrencies() {

		// Set URL params
		var params = new LinkedHashMap<String, String>();
		params.put("ids", "XTC,XEM");
		params.put("format", "json");

		Map<String, Object> requestParams = ImmutableMap.of(
			"currencies", new String[] {"XTC", "XEM"}
		);
		var result = currenciesEndpoint.getMetaData(requestParams);
		assertNotNull(result);
		assertThat("Should have been 2", result.size(), is(2));
		assertNotNull(result.get(0).getDescription());
	}

	@Test
	public void testGetMetaDataWithCurrenciesAndAttributes() {

		// Set URL params
		var params = new LinkedHashMap<String, String>();
		params.put("ids", "XRP,BTT");
		params.put("attributes", "id,name");
		params.put("format", "json");

		Map<String, Object> requestParams = ImmutableMap.of(
			"currencies", new String[] {"XRP", "BTT"},
			"attributes", new MetaAttribute[] { MetaAttribute.ID, MetaAttribute.NAME }
		);
		var result = currenciesEndpoint.getMetaData(requestParams);
		assertNotNull(result);
		assertThat("Should have been 2", result.size(), is(2));
		assertNotNull(result.get(0).getName());
		assertNull(result.get(0).getDescription());
	}

	@Test
	public void testGetMetaDataWithCurrenciesAndAttributesAndFormat() {

		// Set URL params
		var params = new LinkedHashMap<String, String>();
		params.put("ids", "BCH,LTC");
		params.put("attributes", "id,name");
		params.put("format", "csv");

		Map<String, Object> requestParams = ImmutableMap.of(
			"currencies", new String[] {"BCH", "LTC"},
			"attributes", new MetaAttribute[] { MetaAttribute.ID, MetaAttribute.NAME }
		);
		var result = currenciesEndpoint.getMetaDataAsCSV(requestParams);
		assertNotNull(result);
	}

	@Test
	public void testGetCurrenciesSparklineWithStartDate() {

		// Set URL params
		var params = new LinkedHashMap<String, String>();
		params.put("start", "2019-11-10T00:00:00Z");

		Map<String, Object> requestParams = ImmutableMap.of(
			"start", LocalDate.of(2019, Month.NOVEMBER, 10)
		);
		var result = currenciesEndpoint.getCurrenciesSparkline(requestParams);
		assertNotNull(result);
		assertTrue(result.size() > 0);
	}

	@Test
	public void testGetCurrenciesSparklineWithStartDateAndEndDate() {

		// Set URL params
		var params = new LinkedHashMap<String, String>();
		params.put("start", "2019-10-01T00:00:00Z");
		params.put("end", "2019-10-02T00:00:00Z");

		Map<String, Object> requestParams = ImmutableMap.of(
			"start", LocalDate.of(2019, Month.OCTOBER, 1),
			"end", LocalDate.of(2019, Month.OCTOBER, 2)
		);
		var result = currenciesEndpoint.getCurrenciesSparkline(requestParams);
		assertNotNull(result);
		assertTrue(result.size() > 0);
	}
}