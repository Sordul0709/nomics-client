package de.sordul.nomics_client.impl;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import org.apache.commons.io.FileUtils;

import org.mockito.internal.util.reflection.FieldSetter;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.Month;
import java.util.LinkedHashMap;
import java.util.Map;

import com.google.common.collect.ImmutableMap;

import de.sordul.nomics_client.IRESTClient;
import de.sordul.nomics_client.model.Interval;
import de.sordul.nomics_client.model.MetaAttribute;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class CurrenciesEndpointUnitTest {
	private CurrenciesEndpoint currenciesEndpoint;
	private final String apiKey = "yeahForSureIWillPutARealKeyHere";

	@BeforeAll
	public void init() {
		currenciesEndpoint = new CurrenciesEndpoint(apiKey);
	}

	@Test
	public void testGetTickerDataWithCurrencies() throws NoSuchFieldException, SecurityException, IOException {

		// Load prepared JSON response from file
		var json = FileUtils.readFileToString(
			Paths.get("src/test/resources/response_testGetTickerDataWithCurrencies.json").toFile(),
			Charset.forName("UTF-8")
		);

		// Set URL params
		var params = new LinkedHashMap<String, String>();
		params.put("key", this.apiKey);
		params.put("ids", "BTC,ETH,XRP");

		// Set up the IRESTClient within our CurrenciesEndpoint
		var mockedRestClient = mock(IRESTClient.class);
		when(mockedRestClient.callEndpoint("currencies/ticker", params)).thenReturn(json);
		FieldSetter.setField(currenciesEndpoint, currenciesEndpoint.getClass().getDeclaredField("restClient"), mockedRestClient);

		Map<String, Object> requestParams = ImmutableMap.of(
			"currencies", new String[] {"BTC", "ETH", "XRP"}
		);
		var result = currenciesEndpoint.getTickerData(requestParams);
		assertNotNull(result);
		assertThat("Should have been 3", result.size(), is(3));
		assertThat("Should have been 'BTC'", result.get(0).getCurrency(), is("BTC"));
		assertNotNull(result.get(0).getIntervalData());
		assertThat("Should have been '5'", result.get(0).getIntervalData().size(), is(5));
	}

	@Test
	public void testGetTickerDataWithCurrenciesAndInterval() throws NoSuchFieldException, SecurityException, IOException {

		// Load prepared JSON response from file
		var json = FileUtils.readFileToString(
			Paths.get("src/test/resources/response_testGetTickerDataWithCurrenciesAndInterval.json").toFile(),
			Charset.forName("UTF-8")
		);

		// Set URL params
		var params = new LinkedHashMap<String, String>();
		params.put("key", this.apiKey);
		params.put("ids", "TRX,BTT,XMR");
		params.put("interval", "1d,ytd");

		// Set up the IRESTClient within our CurrenciesEndpoint
		var mockedRestClient = mock(IRESTClient.class);
		when(mockedRestClient.callEndpoint("currencies/ticker", params)).thenReturn(json);
		FieldSetter.setField(currenciesEndpoint, currenciesEndpoint.getClass().getDeclaredField("restClient"), mockedRestClient);

		Map<String, Object> requestParams = ImmutableMap.of(
			"currencies", new String[] {"TRX", "BTT", "XMR"},
			"interval", new Interval[] { Interval.DAY, Interval.CURRENT_YEAR }
		);
		var result = currenciesEndpoint.getTickerData(requestParams);
		assertNotNull(result);
		assertThat("Should have been 3", result.size(), is(3));
		assertThat("Should have been 'TRX'", result.get(0).getCurrency(), is("TRX"));
		assertNotNull(result.get(0).getIntervalData());
		assertThat("Should have been '2'", result.get(0).getIntervalData().size(), is(2));
	}

	@Test
	public void testGetTickerDataWithCurrenciesAndIntervalAndConvert() throws NoSuchFieldException, SecurityException, IOException {

		// Load prepared JSON response from file
		var json = FileUtils.readFileToString(
			Paths.get("src/test/resources/response_testGetTickerDataWithCurrenciesAndIntervalAndConvert.json").toFile(),
			Charset.forName("UTF-8")
		);

		// Set URL params
		var params = new LinkedHashMap<String, String>();
		params.put("key", this.apiKey);
		params.put("ids", "BTC");
		params.put("interval", "1d,ytd");
		params.put("convert", "BTC");

		// Set up the IRESTClient within our CurrenciesEndpoint
		var mockedRestClient = mock(IRESTClient.class);
		when(mockedRestClient.callEndpoint("currencies/ticker", params)).thenReturn(json);
		FieldSetter.setField(currenciesEndpoint, currenciesEndpoint.getClass().getDeclaredField("restClient"), mockedRestClient);

		Map<String, Object> requestParams = ImmutableMap.of(
			"currencies", new String[] {"BTC"},
			"interval", new Interval[] { Interval.DAY, Interval.CURRENT_YEAR },
			"convert", "btc"
		);
		var result = currenciesEndpoint.getTickerData(requestParams);
		assertNotNull(result);
		assertThat("Should have been 1", result.size(), is(1));
		assertThat("Should have been 'BCH'", result.get(0).getCurrency(), is("BTC"));
		assertNotNull(result.get(0).getIntervalData());
		assertThat("Should have been '2'", result.get(0).getIntervalData().size(), is(2));
		assertThat("Should have been '1.0'", result.get(0).getPrice(), is(1.0d));
	}

	@Test
	public void testGetMetaDataWithCurrencies() throws NoSuchFieldException, SecurityException, IOException {

		// Load prepared JSON response from file
		var json = FileUtils.readFileToString(
			Paths.get("src/test/resources/response_testGetMetaDataWithCurrencies.json").toFile(),
			Charset.forName("UTF-8")
		);

		// Set URL params
		var params = new LinkedHashMap<String, String>();
		params.put("key", this.apiKey);
		params.put("ids", "XTC,XEM");
		params.put("format", "json");
		
		// Set up the IRESTClient within our CurrenciesEndpoint
		var mockedRestClient = mock(IRESTClient.class);
		when(mockedRestClient.callEndpoint("currencies", params)).thenReturn(json);
		FieldSetter.setField(currenciesEndpoint, currenciesEndpoint.getClass().getDeclaredField("restClient"), mockedRestClient);
		
		Map<String, Object> requestParams = ImmutableMap.of(
			"currencies", new String[] {"XTC", "XEM"}
		);
		var result = currenciesEndpoint.getMetaData(requestParams);
		assertNotNull(result);
		assertThat("Should have been 2", result.size(), is(2));
		assertNotNull(result.get(0).getDescription());
	}
	
	@Test
	public void testGetMetaDataWithCurrenciesAndAttributes() throws NoSuchFieldException, SecurityException, IOException {
		
		// Load prepared JSON response from file
		var json = FileUtils.readFileToString(
			Paths.get("src/test/resources/response_testGetMetaDataWithCurrenciesAndAttributes.json").toFile(),
			Charset.forName("UTF-8")
			);
			
		// Set URL params
		var params = new LinkedHashMap<String, String>();
		params.put("key", this.apiKey);
		params.put("ids", "XRP,BTT");
		params.put("attributes", "id,name");
		params.put("format", "json");

		// Set up the IRESTClient within our CurrenciesEndpoint
		var mockedRestClient = mock(IRESTClient.class);
		when(mockedRestClient.callEndpoint("currencies", params)).thenReturn(json);
		FieldSetter.setField(currenciesEndpoint, currenciesEndpoint.getClass().getDeclaredField("restClient"), mockedRestClient);

		Map<String, Object> requestParams = ImmutableMap.of(
			"currencies", new String[] {"XRP", "BTT"},
			"attributes", new MetaAttribute[] { MetaAttribute.ID, MetaAttribute.NAME }
		);
		var result = currenciesEndpoint.getMetaData(requestParams);
		assertNotNull(result);
		assertThat("Should have been 2", result.size(), is(2));
		assertNotNull(result.get(0).getName());
		assertNull(result.get(0).getDescription());
	}

	@Test
	public void testGetMetaDataAsCSV() throws NoSuchFieldException, SecurityException, IOException {
		
		// Load prepared JSON response from file
		var csvString = FileUtils.readFileToString(
			Paths.get("src/test/resources/response_testGetMetaDataWithCurrenciesAndAttributesAndFormat.csv").toFile(),
			Charset.forName("UTF-8")
		);
			
		// Set URL params
		var params = new LinkedHashMap<String, String>();
		params.put("key", this.apiKey);
		params.put("ids", "BCH,LTC");
		params.put("attributes", "id,name");
		params.put("format", "csv");

		// Set up the IRESTClient within our CurrenciesEndpoint
		var mockedRestClient = mock(IRESTClient.class);
		when(mockedRestClient.callEndpoint("currencies", params)).thenReturn(csvString);
		FieldSetter.setField(currenciesEndpoint, currenciesEndpoint.getClass().getDeclaredField("restClient"), mockedRestClient);

		Map<String, Object> requestParams = ImmutableMap.of(
			"currencies", new String[] {"BCH", "LTC"},
			"attributes", new MetaAttribute[] { MetaAttribute.ID, MetaAttribute.NAME }
		);
		var result = currenciesEndpoint.getMetaDataAsCSV(requestParams);
		assertNotNull(result);
	}

	@Test
	public void testGetCurrenciesSparklineWithStartDate() throws NoSuchFieldException, SecurityException, IOException {
		
		// Load prepared JSON response from file
		var json = FileUtils.readFileToString(
			Paths.get("src/test/resources/response_testGetCurrenciesSparklineWithStartDate.json").toFile(),
			Charset.forName("UTF-8")
		);

		// Set URL params
		var params = new LinkedHashMap<String, String>();
		params.put("key", this.apiKey);
		params.put("start", "2019-11-10T00:00:00Z");
		
		// Set up the IRESTClient within our CurrenciesEndpoint
		var mockedRestClient = mock(IRESTClient.class);
		when(mockedRestClient.callEndpoint("currencies/sparkline", params)).thenReturn(json);
		FieldSetter.setField(currenciesEndpoint, currenciesEndpoint.getClass().getDeclaredField("restClient"), mockedRestClient);

		// The result will base on an end-date of 2019-11-12
		Map<String, Object> requestParams = ImmutableMap.of(
			"start", LocalDate.of(2019, Month.NOVEMBER, 10)
		);
		var result = currenciesEndpoint.getCurrenciesSparkline(requestParams);
		assertNotNull(result);
		assertTrue(result.size() > 0);
	}

	@Test
	public void testGetCurrenciesSparklineWithStartDateAndEndDate() throws NoSuchFieldException, SecurityException, IOException {
		
		// Load prepared JSON response from file
		var json = FileUtils.readFileToString(
			Paths.get("src/test/resources/response_testGetCurrenciesSparklineWithStartDateAndEndDate.json").toFile(),
			Charset.forName("UTF-8")
		);

		// Set URL params
		var params = new LinkedHashMap<String, String>();
		params.put("key", this.apiKey);
		params.put("start", "2019-10-01T00:00:00Z");
		params.put("end", "2019-10-02T00:00:00Z");

		// Set up the IRESTClient within our CurrenciesEndpoint
		var mockedRestClient = mock(IRESTClient.class);
		when(mockedRestClient.callEndpoint("currencies/sparkline", params)).thenReturn(json);
		FieldSetter.setField(currenciesEndpoint, currenciesEndpoint.getClass().getDeclaredField("restClient"), mockedRestClient);

		Map<String, Object> requestParams = ImmutableMap.of(
			"start", LocalDate.of(2019, Month.OCTOBER, 1),
			"end", LocalDate.of(2019, Month.OCTOBER, 2)
		);
		var result = currenciesEndpoint.getCurrenciesSparkline(requestParams);
		assertNotNull(result);
		assertTrue(result.size() > 0);
	}
}


