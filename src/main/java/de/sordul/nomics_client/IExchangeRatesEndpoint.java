package de.sordul.nomics_client;

import java.util.List;
import java.util.Map;

import de.sordul.nomics_client.model.ExchangeRate;
import de.sordul.nomics_client.model.ExchangeRateInterval;

/**
 * Interface for all exchange rate related endpoint-methods.
 */
public interface IExchangeRatesEndpoint {

	/**
	 * Returns a list of ExchangeRate objects.
	 *
	 * @return List of ExchangeRate objects
	 */
	public List<ExchangeRate> getExchangeRates();

	/**
	 * Returns a list of ExchangeRate objects as CSV string.
	 *
	 * @return CSV as string
	 */
	public String getExchangeRatesAsCSV();

	/**
	 * Returns a list of historical ExchangeRate objects. A map of two or more of the following
	 * parameters is expected:
	 * - currency : String (mandatory)
	 * - start : java.time.LocalDate (mandatory)
	 * - end : java.time.LocalDate
	 *
	 * @param params A map of parameter objects
	 * @return List of ExchangeRate objects
	 */
	public List<ExchangeRate> getExchangeRatesHistory(Map<String, Object> params);

	/**
	 * Returns a list of historical exchange rates as CSV string. A map of two or more of the following
	 * parameters is expected:
	 * - currency : String (mandatory)
	 * - start : java.time.LocalDate (mandatory)
	 * - end : java.time.LocalDate
	 *
	 * @param params A map of parameter objects
	 * @return CSV as string
	 */
	public String getExchangeRatesHistoryAsCSV(Map<String, Object> params);

	/**
	 * Returns a list of ExchangeRateInterval objects. A map of one or more of the following
	 * parameters is expected:
	 * - start : java.time.LocalDate (mandatory)
	 * - end : java.time.LocalDate
	 *
	 * @param params A map of parameter objects
	 * @return List of ExchangeRate objects
	 */
	public List<ExchangeRateInterval> getExchangeRatesInterval(Map<String, Object> params);

	/**
	 * Returns a list of exchange rate intervals as CSV string. A map of one or more of the following
	 * parameters is expected:
	 * - start : java.time.LocalDate (mandatory)
	 * - end : java.time.LocalDate
	 *
	 * @param params A map of parameter objects
	 * @return CSV as string
	 */
	public String getExchangeRatesIntervalAsCSV(Map<String, Object> params);
}
