package de.sordul.nomics_client.model;

/**
 * Enum for allowed interval values.
 */
public enum Interval {
	HOUR("1h"),
	DAY("1d"),
	DAYS_7("7d"),
	DAYS_30("30d"),
	DAYS_365("365d"),
	CURRENT_YEAR("ytd");

	private String intervalName;

	Interval(String intervalName) {
		this.intervalName = intervalName;
	}

	public String getIntervalName() {
		return intervalName;
	}
}