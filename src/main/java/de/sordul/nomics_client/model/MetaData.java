package de.sordul.nomics_client.model;

/**
 * Value object for currency meta-data.
 */
public class MetaData {
	private String id;
	private String originalSymbol;
	private String name;
	private String description;
	private String websiteUrl;
	private String logoUrl;
	private String blogUrl;
	private String discordUrl;
	private String facebookUrl;
	private String githubUrl;
	private String mediumUrl;
	private String redditUrl;
	private String telegramUrl;
	private String twitterUrl;
	private String whitepaperUrl;
	private String youtubeUrl;
	private String replacedBy;

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getOriginalSymbol() {
		return originalSymbol;
	}
	public void setOriginalSymbol(String originalSymbol) {
		this.originalSymbol = originalSymbol;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getWebsiteUrl() {
		return websiteUrl;
	}
	public void setWebsiteUrl(String websiteUrl) {
		this.websiteUrl = websiteUrl;
	}
	public String getLogoUrl() {
		return logoUrl;
	}
	public void setLogoUrl(String logoUrl) {
		this.logoUrl = logoUrl;
	}
	public String getBlogUrl() {
		return blogUrl;
	}
	public void setBlogUrl(String blogUrl) {
		this.blogUrl = blogUrl;
	}
	public String getDiscordUrl() {
		return discordUrl;
	}
	public void setDiscordUrl(String discordUrl) {
		this.discordUrl = discordUrl;
	}
	public String getFacebookUrl() {
		return facebookUrl;
	}
	public void setFacebookUrl(String facebookUrl) {
		this.facebookUrl = facebookUrl;
	}
	public String getGithubUrl() {
		return githubUrl;
	}
	public void setGithubUrl(String githubUrl) {
		this.githubUrl = githubUrl;
	}
	public String getMediumUrl() {
		return mediumUrl;
	}
	public void setMediumUrl(String mediumUrl) {
		this.mediumUrl = mediumUrl;
	}
	public String getRedditUrl() {
		return redditUrl;
	}
	public void setRedditUrl(String redditUrl) {
		this.redditUrl = redditUrl;
	}
	public String getTelegramUrl() {
		return telegramUrl;
	}
	public void setTelegramUrl(String telegramUrl) {
		this.telegramUrl = telegramUrl;
	}
	public String getTwitterUrl() {
		return twitterUrl;
	}
	public void setTwitterUrl(String twitterUrl) {
		this.twitterUrl = twitterUrl;
	}
	public String getWhitepaperUrl() {
		return whitepaperUrl;
	}
	public void setWhitepaperUrl(String whitepaperUrl) {
		this.whitepaperUrl = whitepaperUrl;
	}
	public String getYoutubeUrl() {
		return youtubeUrl;
	}
	public void setYoutubeUrl(String youtubeUrl) {
		this.youtubeUrl = youtubeUrl;
	}
	public String getReplacedBy() {
		return replacedBy;
	}
	public void setReplacedBy(String replacedBy) {
		this.replacedBy = replacedBy;
	}

	@Override
	public String toString() {
		return String.format(
			"Id: %s\nOriginal symbol: %s\nName: %s\nDescription: %s\nWebsite URL: %s\nLogo URL: %s\nBlog URL: %s\nDiscord URL: %s\nFacebook URL: %s\nGithub URL: %s\nMedium URL: %s\nReddit URL: %s\nTelegram URL: %s\nTwitter URL: %s\nWhitepaper URL: %s\nYoutube URL: %s\nReplaced by: %s",
			this.id,
			this.originalSymbol,
			this.name,
			this.description,
			this.websiteUrl,
			this.logoUrl,
			this.blogUrl,
			this.discordUrl,
			this.facebookUrl,
			this.githubUrl,
			this.mediumUrl,
			this.redditUrl,
			this.telegramUrl,
			this.twitterUrl,
			this.whitepaperUrl,
			this.youtubeUrl,
			this.replacedBy
		);
	}
}
