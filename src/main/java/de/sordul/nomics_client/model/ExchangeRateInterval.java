package de.sordul.nomics_client.model;

import java.time.Instant;

/**
 * Exchange rate interval value-object.
 */
public class ExchangeRateInterval {
	private String currency;
	private double open;
	private Instant openTimestamp;
	private double close;
	private Instant closeTimestamp;

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public double getOpen() {
		return open;
	}

	public void setOpen(double open) {
		this.open = open;
	}

	public Instant getOpenTimestamp() {
		return openTimestamp;
	}

	public void setOpenTimestamp(Instant openTimestamp) {
		this.openTimestamp = openTimestamp;
	}

	public double getClose() {
		return close;
	}

	public void setClose(double close) {
		this.close = close;
	}

	public Instant getCloseTimestamp() {
		return closeTimestamp;
	}

	public void setCloseTimestamp(Instant closeTimestamp) {
		this.closeTimestamp = closeTimestamp;
	}

	@Override
	public String toString() {
		return String.format(
			"Currency: %s\nOpen: %f\nOpen Timestamp: %s\nClose: %f\nClose timestamp: %s",
			this.currency,
			this.open,
			this.openTimestamp,
			this.close,
			this.closeTimestamp
		);
	}
}
