package de.sordul.nomics_client.model;

import java.time.LocalDate;
import java.util.List;

/**
 * Value object for spark-line entries.
 */
public class SparklineEntry {
	private String currency;
	private List<LocalDate> timestamps;
	private List<Double> prices;

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public List<LocalDate> getTimestamps() {
		return timestamps;
	}

	public void setTimestamps(List<LocalDate> timestamps) {
		this.timestamps = timestamps;
	}

	public List<Double> getPrices() {
		return prices;
	}

	public void setPrices(List<Double> prices) {
		this.prices = prices;
	}

	@Override
	public String toString() {
		return String.format(
			"Currency: %s\nTimestamps: %s\nPrices: %s",
			this.currency,
			this.timestamps.toString(),
			this.prices.toString()
		);
	}
}