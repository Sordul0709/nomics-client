package de.sordul.nomics_client.model;

/**
 * Enum for queryable meta-data attributes.
 */
public enum MetaAttribute {
	ID("id"),
	ORIGINAL_SYMBOL("original_symbol"),
	NAME("name"),
	DESCRIPTION("description"),
	WEBSITE_URL("website_url"),
	LOGO_URL("logo_url"),
	BLOG_URL("blog_url"),
	DISCORD_URL("discord_url"),
	FACEBOOK_URL("facebook_url"),
	GITHUB_URL("github_url"),
	MEDIUM_URL("medium_url"),
	REDDIT_URL("reddit_url"),
	TELEGRAM_URL("telegram_url"),
	TWITTER_URL("twitter_url"),
	WHITEPAPER_URL("whitepaper_url"),
	YOUTUBE_URL("youtube_url"),
	REPLACED_BY("replaced_by");

	private String attributeName;

	MetaAttribute(String attributeName) {
		this.attributeName = attributeName;
	}

	public String getAttributeName() {
		return attributeName;
	}
}
