package de.sordul.nomics_client.model;

/**
 * Value-object for market data.
 */
public class MarketData {
	private String exchange;
	private String market;
	private String base;
	private String quote;

	public String getExchange() {
		return exchange;
	}

	public void setExchange(String exchange) {
		this.exchange = exchange;
	}

	public String getMarket() {
		return market;
	}

	public void setMarket(String market) {
		this.market = market;
	}

	public String getBase() {
		return base;
	}

	public void setBase(String base) {
		this.base = base;
	}

	public String getQuote() {
		return quote;
	}

	public void setQuote(String quote) {
		this.quote = quote;
	}

	@Override
	public String toString() {
		return String.format(
			"Exchange: %s\nMarket: %s\nBase: %s\nQuote: %s",
			this.exchange,
			this.market,
			this.base,
			this.quote
		);
	}
}
