package de.sordul.nomics_client.model;

import java.time.Instant;

/**
 * Value-object for market capitalization history.
 */
public class MarketCapHistory {
	private Instant timestamp;
	private long marketCap;

	public Instant getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Instant timestamp) {
		this.timestamp = timestamp;
	}

	public long getMarketCap() {
		return marketCap;
	}

	public void setMarketCap(long marketCap) {
		this.marketCap = marketCap;
	}

	@Override
	public String toString() {
		return String.format(
			"Timestamp: %s\nMarket Cap: %d",
			this.timestamp.toString(),
			this.marketCap
		);
	}
}