package de.sordul.nomics_client.model;

import java.time.Instant;

/**
 * Value object for volume data.
 */
public class Volume {
	private Instant timestamp;
	private long volume;

	public Instant getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Instant timestamp) {
		this.timestamp = timestamp;
	}

	public long getVolume() {
		return volume;
	}

	public void setVolume(long volume) {
		this.volume = volume;
	}

	@Override
	public String toString() {
		return String.format(
			"Timestamp: %s\nVolume: %d",
			this.timestamp.toString(),
			this.volume
		);
	}
}