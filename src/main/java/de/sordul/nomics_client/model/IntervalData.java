package de.sordul.nomics_client.model;

/**
 * Value object for interval data within TickerData objects.
 */
public class IntervalData {
	private double priceChange;
	private double priceChangePct;
	private double volume;
	private double volumeChange;
	private double volumeChangePct;
	private double marketCapChange;
	private double marketCapChangePct;
	private String volumeTransparencyGrade;

	public double getPriceChange() {
		return priceChange;
	}

	public void setPriceChange(double priceChange) {
		this.priceChange = priceChange;
	}

	public double getPriceChangePct() {
		return priceChangePct;
	}

	public void setPriceChangePct(double priceChangePct) {
		this.priceChangePct = priceChangePct;
	}

	public double getVolume() {
		return volume;
	}

	public void setVolume(double volume) {
		this.volume = volume;
	}

	public double getVolumeChange() {
		return volumeChange;
	}

	public void setVolumeChange(double volumeChange) {
		this.volumeChange = volumeChange;
	}

	public double getVolumeChangePct() {
		return volumeChangePct;
	}

	public void setVolumeChangePct(double volumeChangePct) {
		this.volumeChangePct = volumeChangePct;
	}

	public double getMarketCapChange() {
		return marketCapChange;
	}

	public void setMarketCapChange(double marketCapChange) {
		this.marketCapChange = marketCapChange;
	}

	public double getMarketCapChangePct() {
		return marketCapChangePct;
	}

	public void setMarketCapChangePct(double marketCapChangePct) {
		this.marketCapChangePct = marketCapChangePct;
	}

	public String getVolumeTransparencyGrade() {
		return volumeTransparencyGrade;
	}

	public void setVolumeTransparencyGrade(String volumeTransparencyGrade) {
		this.volumeTransparencyGrade = volumeTransparencyGrade;
	}

	@Override
	public String toString() {
		return String.format(
			"Price change: %f\nPrice change (%%): %f\nVolume: %f\nVolume change: %f\nVolume change (%%): %f\nMarket cap change: %f\nMarket cap change (%%): %f\nVolume transparency grade: %s",
			this.priceChange,
			this.priceChangePct,
			this.volume,
			this.volumeChange,
			this.volumeChangePct,
			this.marketCapChange,
			this.marketCapChangePct,
			this.volumeTransparencyGrade
		);
	}
}
