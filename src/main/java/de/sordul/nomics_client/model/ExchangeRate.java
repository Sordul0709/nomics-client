package de.sordul.nomics_client.model;

import java.time.Instant;

/**
 * Value-object for exchange rates.
 */
public class ExchangeRate {
	private String currency;
	private double rate;
	private Instant timestamp;

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public double getRate() {
		return rate;
	}

	public void setRate(double rate) {
		this.rate = rate;
	}

	public Instant getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Instant timestamp) {
		this.timestamp = timestamp;
	}

	@Override
	public String toString() {
		return String.format(
			"Currency: %s\nRate: %f\nTimestamp: %s",
			this.currency,
			this.rate,
			this.timestamp
		);
	}
}
