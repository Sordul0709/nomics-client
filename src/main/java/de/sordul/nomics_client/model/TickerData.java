package de.sordul.nomics_client.model;

import java.time.Instant;
import java.util.Map;

/**
 * Value object for ticker data.
 */
public class TickerData {
	private String currency;
	private String id;
	private double price;
	private Instant priceDate;
	private String symbol;
	private long circulatingSupply;
	private long maxSupply;
	private String name;
	private String logoUrl;
	private long marketCap;
	private int rank;
	private double high;
	private Instant highTimestamp;
	private Map<Interval, IntervalData> intervalData;

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public Instant getPriceDate() {
		return priceDate;
	}

	public void setPriceDate(Instant price_date) {
		this.priceDate = price_date;
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public long getCirculatingSupply() {
		return circulatingSupply;
	}

	public void setCirculatingSupply(long circulating_supply) {
		this.circulatingSupply = circulating_supply;
	}

	public long getMaxSupply() {
		return maxSupply;
	}

	public void setMaxSupply(long max_supply) {
		this.maxSupply = max_supply;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLogoUrl() {
		return logoUrl;
	}

	public void setLogoUrl(String logo_url) {
		this.logoUrl = logo_url;
	}

	public long getMarketCap() {
		return marketCap;
	}

	public void setMarketCap(long market_cap) {
		this.marketCap = market_cap;
	}

	public int getRank() {
		return rank;
	}

	public void setRank(int rank) {
		this.rank = rank;
	}

	public double getHigh() {
		return high;
	}

	public void setHigh(double high) {
		this.high = high;
	}

	public Instant getHighTimestamp() {
		return highTimestamp;
	}

	public void setHighTimestamp(Instant high_timestamp) {
		this.highTimestamp = high_timestamp;
	}

	public Map<Interval, IntervalData> getIntervalData() {
		return intervalData;
	}

	public void setIntervalData(Map<Interval, IntervalData> intervalData) {
		this.intervalData = intervalData;
	}

	@Override
	public String toString() {
		return String.format(
			"Currency: %s\nID: %s\nPrice: %f\nPrice date: %s\nSymbol: %s\nCirculating supply: %d\nMax supply: %d\nName: %s\nLogo URL: %s\nMarket Cap: %d\nRank: %d\nHigh: %f\nHigh timestamp: %s",
			this.currency,
			this.id,
			this.price,
			this.priceDate.toString(),
			this.symbol,
			this.circulatingSupply,
			this.maxSupply,
			this.name,
			this.logoUrl,
			this.marketCap,
			this.rank,
			this.high,
			this.highTimestamp.toString()
		);
	}
}
