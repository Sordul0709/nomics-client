package de.sordul.nomics_client.impl;

/**
 * Umbrella class providing access to all endpoints.
 */
public class NomicsClient {
	public final CurrenciesEndpoint currencies = new CurrenciesEndpoint();
	public final MarketsEndpoint markets = new MarketsEndpoint();
	public final VolumeEndpoint volume = new VolumeEndpoint();
	public final ExchangeRatesEndpoint rates = new ExchangeRatesEndpoint();

	/**
	 * Instantiates all Endpoints with given API key.
	 *
	 * @param apiKey Nomics API key
	 */
	public NomicsClient(String apiKey) {
		currencies.setKey(apiKey);
		markets.setKey(apiKey);
		volume.setKey(apiKey);
		rates.setKey(apiKey);
	}
}