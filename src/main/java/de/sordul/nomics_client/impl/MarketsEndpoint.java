package de.sordul.nomics_client.impl;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import de.sordul.nomics_client.IMarketsEndpoint;
import de.sordul.nomics_client.IRESTClient;
import de.sordul.nomics_client.impl.deserializer.MarketCapHistoryDeserializer;
import de.sordul.nomics_client.model.MarketCapHistory;
import de.sordul.nomics_client.model.MarketData;

/**
 * Implementation of the markets endpoint interface.
 */
public class MarketsEndpoint implements IMarketsEndpoint {
	private String key;
	private Gson gson;
	private IRESTClient restClient;

	/**
	 * Constructs an instance with a blank API key.
	 */
	public MarketsEndpoint() {
		this.key = "";
		this.restClient = RESTClient.getInstance();
		this.gson = new GsonBuilder()
			.registerTypeAdapter(MarketCapHistory.class, new MarketCapHistoryDeserializer())
			.create();
	}

	/**
	 * Constructs an instance and sets the API key for all subsequent requests.
	 *
	 * @param key API key
	 */
	public MarketsEndpoint(String key) {
		this();
		this.key = key;
	}

	@Override
	public List<MarketData> getMarketData(Map<String, Object> params) {
		var collectionType = new TypeToken<List<MarketData>>(){}.getType();
		var requestParams = new LinkedHashMap<String, String>();
		List<MarketData> result;
		String resultString;

		// API key
		requestParams.put("key", this.key);

		// Exchange
		if (params.containsKey("exchange")) {
			var exchange = (String) params.get("exchange");
			requestParams.put("exchange", exchange);
		}

		// Base
		if (params.containsKey("base")) {
			var base = (String[]) params.get("base");
			requestParams.put("base", String.join(",", base));
		}

		// Quote
		if (params.containsKey("quote")) {
			var quote = (String[]) params.get("quote");
			requestParams.put("quote", String.join(",", quote));
		}

		// Call endpoint, deserialize the JSON to a list of TickerData objects
		resultString = restClient.callEndpoint("markets", requestParams);
		result = gson.fromJson(resultString, collectionType);
		return result;
	}

	@Override
	public String getMarketDataAsCSV(Map<String, Object> params) {
		var requestParams = new LinkedHashMap<String, String>();
		String resultString;

		// API key
		requestParams.put("key", this.key);

		// Exchange
		if (params.containsKey("exchange")) {
			var exchange = (String) params.get("exchange");
			requestParams.put("exchange", exchange);
		}

		// Base
		if (params.containsKey("base")) {
			var base = (String[]) params.get("base");
			requestParams.put("base", String.join(",", base));
		}

		// Quote
		if (params.containsKey("quote")) {
			var quote = (String[]) params.get("quote");
			requestParams.put("quote", String.join(",", quote));
		}

		// Format
		requestParams.put("format", "csv");

		// Call endpoint, deserialize the JSON to a list of TickerData objects
		resultString = restClient.callEndpoint("markets", requestParams);
		return resultString;
	}

	@Override
	public List<MarketCapHistory> getMarketCapHistory(Map<String, Object> params) {
		var formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		var rfc3339tail = "T00:00:00Z";
		var collectionType = new TypeToken<List<MarketCapHistory>>(){}.getType();
		var requestParams = new LinkedHashMap<String, String>();
		List<MarketCapHistory> result;
		String resultString;

		// API key
		requestParams.put("key", this.key);

		// Start date (mandatory)
		var start = (LocalDate) params.get("start");
		requestParams.put("start", formatter.format(start).concat(rfc3339tail));
		
		// End date
		if (params.containsKey("end")) {
			var end = (LocalDate) params.get("end");
			requestParams.put("end", formatter.format(end).concat(rfc3339tail));
		}

		// Call endpoint, deserialize the JSON to a list of TickerData objects
		resultString = restClient.callEndpoint("market-cap/history", requestParams);
		result = gson.fromJson(resultString, collectionType);
		return result;
	}

	@Override
	public String getMarketCapHistoryAsCSV(Map<String, Object> params) {
		var formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		var rfc3339tail = "T00:00:00Z";
		var requestParams = new LinkedHashMap<String, String>();
		String resultString;

		// API key
		requestParams.put("key", this.key);

		// Start date (mandatory)
		var start = (LocalDate) params.get("start");
		requestParams.put("start", formatter.format(start).concat(rfc3339tail));
		
		// End date
		if (params.containsKey("end")) {
			var end = (LocalDate) params.get("end");
			requestParams.put("end", formatter.format(end).concat(rfc3339tail));
		}

		// Format
		requestParams.put("format", "csv");

		// Call endpoint, and return CSV string
		resultString = restClient.callEndpoint("market-cap/history", requestParams);
		return resultString;
	}

	/**
	 * Returns the used API key.
	 *
	 * @return API key
	 */
	public String getKey() {
		return key;
	}

	/**
	 * Sets the API to use.
	 *
	 * @param key API key
	 */
	public void setKey(String key) {
		this.key = key;
	}
}