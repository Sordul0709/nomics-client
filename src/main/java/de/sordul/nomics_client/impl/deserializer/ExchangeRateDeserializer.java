package de.sordul.nomics_client.impl.deserializer;

import java.lang.reflect.Type;
import java.time.Instant;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import de.sordul.nomics_client.model.ExchangeRate;

/**
 * Deserializer for ExchangeRate JSON objects.
 */
public class ExchangeRateDeserializer implements JsonDeserializer<ExchangeRate> {

	@Override
	public ExchangeRate deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
			throws JsonParseException {

		var jsonObject = json.getAsJsonObject();
		var result = new ExchangeRate();

		if (jsonObject.has("currency")) result.setCurrency(jsonObject.get("currency").getAsString());
		if (jsonObject.has("rate")) result.setRate(jsonObject.get("rate").getAsDouble());
		if (jsonObject.has("timestamp")) result.setTimestamp(Instant.parse(jsonObject.get("timestamp").getAsString()));

		return result;
	}

}