package de.sordul.nomics_client.impl.deserializer;

import java.lang.reflect.Type;
import java.time.Instant;
import java.util.HashMap;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import de.sordul.nomics_client.model.Interval;
import de.sordul.nomics_client.model.IntervalData;
import de.sordul.nomics_client.model.TickerData;

/**
 * Custom deserializer for ticker data
 */
public class TickerDataDeserializer implements JsonDeserializer<TickerData> {
	private static final Gson gson = new GsonBuilder()
		.registerTypeAdapter(IntervalData.class, new IntervalDataDeserializer())
		.create();

	@Override
	public TickerData deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
			throws JsonParseException {
		var jsonObject = json.getAsJsonObject();
		var result = new TickerData();

		if (jsonObject.has("currency")) result.setCurrency(jsonObject.get("currency").getAsString());
		if (jsonObject.has("id")) result.setId(jsonObject.get("id").getAsString());
		if (jsonObject.has("price")) result.setPrice(jsonObject.get("price").getAsDouble());
		if (jsonObject.has("price_date")) {
			result.setPriceDate(Instant.parse(jsonObject.get("price_date").getAsString()));
		}
		if (jsonObject.has("symbol")) result.setSymbol(jsonObject.get("symbol").getAsString());
		if (jsonObject.has("circulating_supply")) result.setCirculatingSupply(jsonObject.get("circulating_supply").getAsLong());
		if (jsonObject.has("max_supply")) result.setMaxSupply(jsonObject.get("max_supply").getAsLong());
		if (jsonObject.has("name")) result.setName(jsonObject.get("name").getAsString());
		if (jsonObject.has("logo_url")) result.setLogoUrl(jsonObject.get("logo_url").getAsString());
		if (jsonObject.has("market_cap")) result.setMarketCap(jsonObject.get("market_cap").getAsLong());
		if (jsonObject.has("rank")) result.setRank(jsonObject.get("rank").getAsInt());
		if (jsonObject.has("high")) result.setHigh(jsonObject.get("high").getAsDouble());
		if (jsonObject.has("high_timestamp")) {
			result.setHighTimestamp(Instant.parse(jsonObject.get("high_timestamp").getAsString()));
		}

		// Add interval data if present
		result.setIntervalData(new HashMap<>());
		if (jsonObject.has("1d")) {
			result.getIntervalData().put(Interval.DAY, gson.fromJson(jsonObject.get("1d"), IntervalData.class));
		}
		if (jsonObject.has("7d")) {
			result.getIntervalData().put(Interval.DAYS_7, gson.fromJson(jsonObject.get("7d"), IntervalData.class));
		}
		if (jsonObject.has("30d")) {
			result.getIntervalData().put(Interval.DAYS_30, gson.fromJson(jsonObject.get("30d"), IntervalData.class));
		}
		if (jsonObject.has("365d")) {
			result.getIntervalData().put(Interval.DAYS_365, gson.fromJson(jsonObject.get("365d"), IntervalData.class));
		}
		if (jsonObject.has("ytd")) {
			result.getIntervalData().put(Interval.CURRENT_YEAR, gson.fromJson(jsonObject.get("ytd"), IntervalData.class));
		}

		return result;
	}

}