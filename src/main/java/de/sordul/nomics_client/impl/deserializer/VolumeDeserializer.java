package de.sordul.nomics_client.impl.deserializer;

import java.lang.reflect.Type;
import java.time.Instant;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import de.sordul.nomics_client.model.Volume;

/**
 * Custom deserializer for Volume data.
 */
public class VolumeDeserializer implements JsonDeserializer<Volume> {

	@Override
	public Volume deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
			throws JsonParseException {

			var jsonObject = json.getAsJsonObject();
			var result = new Volume();
	
			if (jsonObject.has("volume")) result.setVolume(jsonObject.get("volume").getAsLong());
			if (jsonObject.has("timestamp")) result.setTimestamp(Instant.parse(jsonObject.get("timestamp").getAsString()));
	
			return result;
	}

}