package de.sordul.nomics_client.impl.deserializer;

import java.lang.reflect.Type;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import de.sordul.nomics_client.model.SparklineEntry;

/**
 * Custom deserializer for sparkline entries.
 */
public class SparklineEntryDeserializer implements JsonDeserializer<SparklineEntry> {

	@Override
	public SparklineEntry deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
		var jsonObject = json.getAsJsonObject();
		var result = new SparklineEntry();

		if (jsonObject.has("currency")) result.setCurrency(jsonObject.get("currency").getAsString());
		if (jsonObject.has("timestamps")) {
			var timestampList = new ArrayList<LocalDate>();
			var timestampItr = jsonObject.get("timestamps").getAsJsonArray().iterator();

			while (timestampItr.hasNext()) {
				var timestmapStr = timestampItr.next().getAsString();
				var timestampDate = LocalDate.ofInstant(Instant.parse(timestmapStr), ZoneId.systemDefault());
				timestampList.add(timestampDate);
			}

			result.setTimestamps(timestampList);
		}
		if (jsonObject.has("prices")) {
			var pricesList = new ArrayList<Double>();
			var pricesItr = jsonObject.get("prices").getAsJsonArray().iterator();

			while (pricesItr.hasNext()) {
				pricesList.add(pricesItr.next().getAsDouble());
			}

			result.setPrices(pricesList);
		}

		return result;
	}
}