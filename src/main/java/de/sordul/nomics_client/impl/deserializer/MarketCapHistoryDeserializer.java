package de.sordul.nomics_client.impl.deserializer;

import java.lang.reflect.Type;
import java.time.Instant;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import de.sordul.nomics_client.model.MarketCapHistory;

/**
 * Custom deserializer for MarketCapHistory data.
 */
public class MarketCapHistoryDeserializer implements JsonDeserializer<MarketCapHistory> {

	@Override
	public MarketCapHistory deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
			throws JsonParseException {

		var jsonObject = json.getAsJsonObject();
		var result = new MarketCapHistory();

		if (jsonObject.has("market_cap")) result.setMarketCap(jsonObject.get("market_cap").getAsLong());
		if (jsonObject.has("timestamp")) result.setTimestamp(Instant.parse(jsonObject.get("timestamp").getAsString()));

		return result;
	}
}