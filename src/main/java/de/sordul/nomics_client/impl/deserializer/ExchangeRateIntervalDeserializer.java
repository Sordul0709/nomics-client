package de.sordul.nomics_client.impl.deserializer;

import java.lang.reflect.Type;
import java.time.Instant;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import de.sordul.nomics_client.model.ExchangeRateInterval;

/**
 * Deserializer for ExchangeRateInterval JSON objects.
 */
public class ExchangeRateIntervalDeserializer implements JsonDeserializer<ExchangeRateInterval> {

	@Override
	public ExchangeRateInterval deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
			throws JsonParseException {

		var jsonObject = json.getAsJsonObject();
		var result = new ExchangeRateInterval();

		if (jsonObject.has("currency")) result.setCurrency(jsonObject.get("currency").getAsString());
		if (jsonObject.has("open")) result.setOpen(jsonObject.get("open").getAsDouble());
		if (jsonObject.has("open_timestamp")) result.setOpenTimestamp(Instant.parse(jsonObject.get("open_timestamp").getAsString()));
		if (jsonObject.has("close")) result.setClose(jsonObject.get("close").getAsDouble());
		if (jsonObject.has("close_timestamp")) result.setCloseTimestamp(Instant.parse(jsonObject.get("close_timestamp").getAsString()));

		return result;
	}

}