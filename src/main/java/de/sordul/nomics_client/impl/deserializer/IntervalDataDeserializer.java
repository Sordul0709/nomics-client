package de.sordul.nomics_client.impl.deserializer;

import java.lang.reflect.Type;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import de.sordul.nomics_client.model.IntervalData;

/**
 * Custom deserializer for interval data.
 */
public class IntervalDataDeserializer implements JsonDeserializer<IntervalData> {

	@Override
	public IntervalData deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
		var jsonObject = json.getAsJsonObject();
		var result = new IntervalData();

		if (jsonObject.has("price_change")) result.setPriceChange(jsonObject.get("price_change").getAsDouble());
		if (jsonObject.has("price_change_pct")) result.setPriceChangePct(jsonObject.get("price_change_pct").getAsDouble());
		if (jsonObject.has("volume")) result.setVolume(jsonObject.get("volume").getAsDouble());
		if (jsonObject.has("volume_change")) result.setVolumeChange(jsonObject.get("volume_change").getAsDouble());
		if (jsonObject.has("volume_change_pct")) result.setVolumeChangePct(jsonObject.get("volume_change_pct").getAsDouble());
		if (jsonObject.has("market_cap_change")) result.setMarketCapChange(jsonObject.get("market_cap_change").getAsDouble());
		if (jsonObject.has("market_cap_change_pct")) result.setMarketCapChangePct(jsonObject.get("market_cap_change_pct").getAsDouble());
		if (jsonObject.has("volume_transparency_grade")) result.setVolumeTransparencyGrade(jsonObject.get("volume_transparency_grade").getAsString());

		return result;
	}
}