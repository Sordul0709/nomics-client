package de.sordul.nomics_client.impl;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import de.sordul.nomics_client.IRESTClient;
import de.sordul.nomics_client.IVolumeEndpoint;
import de.sordul.nomics_client.impl.deserializer.VolumeDeserializer;
import de.sordul.nomics_client.model.Volume;

public class VolumeEndpoint implements IVolumeEndpoint {
	private String key;
	private Gson gson;
	private IRESTClient restClient;

	/**
	 * Constructs an instance with a blank API key.
	 */
	public VolumeEndpoint() {
		this.key = "";
		this.restClient = RESTClient.getInstance();
		this.gson = new GsonBuilder()
			.registerTypeAdapter(Volume.class, new VolumeDeserializer())
			.create();
	}

	/**
	 * Constructs an instance and sets the API key for all subsequent requests.
	 *
	 * @param key API key
	 */
	public VolumeEndpoint(String key) {
		this();
		this.key = key;
	}

	@Override
	public List<Volume> getVolumeHistory(Map<String, Object> params) {
		var formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		var rfc3339tail = "T00:00:00Z";
		var collectionType = new TypeToken<List<Volume>>(){}.getType();
		var requestParams = new LinkedHashMap<String, String>();
		List<Volume> result;
		String resultString;

		// API key
		requestParams.put("key", this.key);

		// Start date (mandatory)
		var start = (LocalDate) params.get("start");
		requestParams.put("start", formatter.format(start).concat(rfc3339tail));
		
		// End date
		if (params.containsKey("end")) {
			var end = (LocalDate) params.get("end");
			requestParams.put("end", formatter.format(end).concat(rfc3339tail));
		}

		// Call endpoint, deserialize the JSON to a list of TickerData objects
		resultString = restClient.callEndpoint("volume/history", requestParams);
		result = gson.fromJson(resultString, collectionType);
		return result;
	}

	@Override
	public String getVolumeHistoryAsCSV(Map<String, Object> params) {
		var formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		var rfc3339tail = "T00:00:00Z";
		var requestParams = new LinkedHashMap<String, String>();
		String resultString;

		// API key
		requestParams.put("key", this.key);

		// Start date (mandatory)
		var start = (LocalDate) params.get("start");
		requestParams.put("start", formatter.format(start).concat(rfc3339tail));
		
		// End date
		if (params.containsKey("end")) {
			var end = (LocalDate) params.get("end");
			requestParams.put("end", formatter.format(end).concat(rfc3339tail));
		}

		// Format
		requestParams.put("format", "csv");

		// Call endpoint, deserialize the JSON to a list of TickerData objects
		resultString = restClient.callEndpoint("volume/history", requestParams);
		return resultString;
	}

	/**
	 * Returns the used API key.
	 *
	 * @return API key
	 */
	public String getKey() {
		return key;
	}

	/**
	 * Sets the API to use.
	 *
	 * @param key API key
	 */
	public void setKey(String key) {
		this.key = key;
	}
}