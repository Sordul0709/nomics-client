package de.sordul.nomics_client.impl;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import de.sordul.nomics_client.ICurrenciesEndpoint;
import de.sordul.nomics_client.IRESTClient;
import de.sordul.nomics_client.impl.deserializer.IntervalDataDeserializer;
import de.sordul.nomics_client.impl.deserializer.SparklineEntryDeserializer;
import de.sordul.nomics_client.impl.deserializer.TickerDataDeserializer;
import de.sordul.nomics_client.model.Interval;
import de.sordul.nomics_client.model.IntervalData;
import de.sordul.nomics_client.model.MetaAttribute;
import de.sordul.nomics_client.model.MetaData;
import de.sordul.nomics_client.model.SparklineEntry;
import de.sordul.nomics_client.model.TickerData;

/**
 * Implementation of the currencies endpoint interface.
 */
public class CurrenciesEndpoint implements ICurrenciesEndpoint {
	private IRESTClient restClient;
	private Gson gson;
	private String key;

	/**
	 * Constructs an instance with a blank API key.
	 */
	public CurrenciesEndpoint() {
		this.key = "";
		this.restClient = RESTClient.getInstance();
		this.gson = new GsonBuilder()
			.registerTypeAdapter(TickerData.class, new TickerDataDeserializer())
			.registerTypeAdapter(IntervalData.class, new IntervalDataDeserializer())
			.registerTypeAdapter(SparklineEntry.class, new SparklineEntryDeserializer())
			.create();
	}

	/**
	 * Constructs an instance and sets the API key for all subsequent requests.
	 *
	 * @param key API key
	 */
	public CurrenciesEndpoint(String key) {
		this();
		this.key = key;
	}

	@Override
	public List<TickerData> getTickerData(Map<String, Object> params) {
		var collectionType = new TypeToken<List<TickerData>>(){}.getType();
		var requestParams = new LinkedHashMap<String, String>();
		List<TickerData> result;
		String resultString;

		// API key
		requestParams.put("key", this.key);

		// Currencies
		if (params.containsKey("currencies")) {
			var ids = (String[]) params.get("currencies");
			requestParams.put("ids", String.join(",", ids));
		}

		// Interval
		if (params.containsKey("interval")) {
			var interval = (Interval[]) params.get("interval");
			var intervalParam = Arrays
				.stream(interval)
				.map(i -> i.getIntervalName())
				.collect(Collectors.joining(","));
			requestParams.put("interval", intervalParam);
		}

		// Convert
		if (params.containsKey("convert")) {
			var convert = (String) params.get("convert");
			requestParams.put("convert", convert.toUpperCase());
		}

		// Call endpoint, deserialize the JSON to a list of TickerData objects
		resultString = restClient.callEndpoint("currencies/ticker", requestParams);
		result = gson.fromJson(resultString, collectionType);
		return result;
	}

	@Override
	public List<MetaData> getMetaData(Map<String, Object> params) {
		var collectionType = new TypeToken<List<MetaData>>(){}.getType();
		var requestParams = new LinkedHashMap<String, String>();
		String result;

		// API key
		requestParams.put("key", this.key);

		// Currencies
		if (params.containsKey("currencies")) {
			var ids = (String[]) params.get("currencies");
			requestParams.put("ids", String.join(",", ids));
		}
		
		// Attributes
		if (params.containsKey("attributes")) {
			var attributes = (MetaAttribute[]) params.get("attributes");
			var attributesParam = Arrays
			.stream(attributes)
			.map(i -> i.getAttributeName())
			.collect(Collectors.joining(","));
			requestParams.put("attributes", attributesParam);
		}

		// Set format parameter
		requestParams.put("format", "json");

		// Call endpoint, deserialize the JSON to a list of TickerData objects
		result = restClient.callEndpoint("currencies", requestParams);
		return gson.fromJson(result, collectionType);
	}

	@Override
	public String getMetaDataAsCSV(Map<String, Object> params) {
		var requestParams = new LinkedHashMap<String, String>();
		String result;

		// API key
		requestParams.put("key", this.key);

		// Currencies
		if (params.containsKey("currencies")) {
			var ids = (String[]) params.get("currencies");
			requestParams.put("ids", String.join(",", ids));
		}
		
		// Attributes
		if (params.containsKey("attributes")) {
			var attributes = (MetaAttribute[]) params.get("attributes");
			var attributesParam = Arrays
			.stream(attributes)
			.map(i -> i.getAttributeName())
			.collect(Collectors.joining(","));
			requestParams.put("attributes", attributesParam);
		}

		// Set format parameter
		requestParams.put("format", "csv");

		// Call endpoint, deserialize the JSON to a list of TickerData objects
		result = restClient.callEndpoint("currencies", requestParams);
		return result;
	}

	@Override
	public List<SparklineEntry> getCurrenciesSparkline(Map<String, Object> params) {
		var formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		var rfc3339tail = "T00:00:00Z";
		var collectionType = new TypeToken<List<SparklineEntry>>(){}.getType();
		var requestParams = new LinkedHashMap<String, String>();
		List<SparklineEntry> result;
		String resultString;

		// API key
		requestParams.put("key", this.key);

		// Start date (mandatory)
		var start = (LocalDate) params.get("start");
		requestParams.put("start", formatter.format(start).concat(rfc3339tail));
		
		// End date
		if (params.containsKey("end")) {
			var end = (LocalDate) params.get("end");
			requestParams.put("end", formatter.format(end).concat(rfc3339tail));
		}

		// Call endpoint, deserialize the JSON to a list of TickerData objects
		resultString = restClient.callEndpoint("currencies/sparkline", requestParams);
		result = gson.fromJson(resultString, collectionType);
		return result;
	}

	/**
	 * Returns the used API key.
	 *
	 * @return API key
	 */
	public String getKey() {
		return key;
	}

	/**
	 * Sets the API to use.
	 *
	 * @param key API key
	 */
	public void setKey(String key) {
		this.key = key;
	}
}