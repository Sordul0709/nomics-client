package de.sordul.nomics_client.impl;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import de.sordul.nomics_client.IRESTClient;

/**
 * Default implementation of the IRESTClient interface.
 */
public class RESTClient implements IRESTClient {
	private static final String BASE_URL = "https://api.nomics.com/v1/";
	private HttpGet request;
	private CloseableHttpClient httpClient = HttpClients.createDefault();
	private static RESTClient client = null;

	/**
	 * Returns an instance of the REST client (singleton).
	 *
	 * @return RESTClient instance
	 */
	public static RESTClient getInstance() {
		if (client == null) {
			client = new RESTClient();
		}
		return client;
	}

	/**
	 * Default constructor is private; use getInstance
	 * for a client instance.
	 */ 
	private RESTClient() {}

	@Override
	public String callEndpoint(String urlFragment, Map<String, String> urlParams) {
		String result = null;
		CloseableHttpResponse response;
		HttpEntity entity;
		request = new HttpGet(BASE_URL + urlFragment + getParamsString(urlParams));
		
		try {
			response = httpClient.execute(request);
			entity = response.getEntity();
			
			result = EntityUtils.toString(entity);
			response.close();

		} catch (IOException e) {
			e.printStackTrace();
		}

		return result;
	}

	/**
	 * Builds a HTTP parameter string based on given map.
	 *
	 * @param params Map of desired HTTP parameters
	 */
	private String getParamsString(Map<String, String> params) {
		StringBuilder result = new StringBuilder();
		String resultString;

		try {
			for (Map.Entry<String, String> entry : params.entrySet()) {
				result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
				result.append("=");
				result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
				result.append("&");
			}

		// Obligatory; UTF-8 has to be supported
		} catch (UnsupportedEncodingException e) {
			return "";
		}

		resultString = result.toString();
		return resultString.length() > 0
			? "?" + resultString.substring(0, resultString.length() - 1)
			: resultString;
	}
}