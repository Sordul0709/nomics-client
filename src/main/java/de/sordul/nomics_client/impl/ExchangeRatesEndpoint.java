package de.sordul.nomics_client.impl;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import de.sordul.nomics_client.IExchangeRatesEndpoint;
import de.sordul.nomics_client.IRESTClient;
import de.sordul.nomics_client.impl.deserializer.ExchangeRateDeserializer;
import de.sordul.nomics_client.impl.deserializer.ExchangeRateIntervalDeserializer;
import de.sordul.nomics_client.model.ExchangeRate;
import de.sordul.nomics_client.model.ExchangeRateInterval;

public class ExchangeRatesEndpoint implements IExchangeRatesEndpoint {
	private String key;
	private Gson gson;
	private IRESTClient restClient;

	/**
	 * Constructs an instance with a blank API key.
	 */
	public ExchangeRatesEndpoint() {
		this.key = "";
		this.restClient = RESTClient.getInstance();
		this.gson = new GsonBuilder()
			.registerTypeAdapter(ExchangeRate.class, new ExchangeRateDeserializer())
			.registerTypeAdapter(ExchangeRateInterval.class, new ExchangeRateIntervalDeserializer())
			.create();
	}

	/**
	 * Constructs an instance and sets the API key for all subsequent requests.
	 *
	 * @param key API key
	 */
	public ExchangeRatesEndpoint(String key) {
		this();
		this.key = key;
	}

	@Override
	public List<ExchangeRate> getExchangeRates() {
		var collectionType = new TypeToken<List<ExchangeRate>>(){}.getType();
		var requestParams = new LinkedHashMap<String, String>();
		List<ExchangeRate> result;
		String resultString;

		// API key
		requestParams.put("key", this.key);

		// Call endpoint, deserialize the JSON to a list of TickerData objects
		resultString = restClient.callEndpoint("exchange-rates", requestParams);
		result = gson.fromJson(resultString, collectionType);
		return result;
	}

	@Override
	public String getExchangeRatesAsCSV() {
		var requestParams = new LinkedHashMap<String, String>();
		String resultString;

		// API key
		requestParams.put("key", this.key);

		// Format
		requestParams.put("format", "csv");

		// Call endpoint, and return CSV string
		resultString = restClient.callEndpoint("exchange-rates", requestParams);
		return resultString;
	}

	@Override
	public List<ExchangeRate> getExchangeRatesHistory(Map<String, Object> params) {
		var formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		var rfc3339tail = "T00:00:00Z";
		var collectionType = new TypeToken<List<ExchangeRate>>(){}.getType();
		var requestParams = new LinkedHashMap<String, String>();
		List<ExchangeRate> result;
		String resultString;

		// API key
		requestParams.put("key", this.key);

		// Currency
		if (params.containsKey("currency")) {
			var exchange = (String) params.get("currency");
			requestParams.put("currency", exchange);
		}

		// Start date
		var start = (LocalDate) params.get("start");
		requestParams.put("start", formatter.format(start).concat(rfc3339tail));

		// End date
		if (params.containsKey("quote")) {
			var end = (LocalDate) params.get("end");
			requestParams.put("end", formatter.format(end).concat(rfc3339tail));
		}

		// Call endpoint, deserialize the JSON to a list of ExchangeRate objects
		resultString = restClient.callEndpoint("exchange-rates/history", requestParams);
		result = gson.fromJson(resultString, collectionType);
		return result;
	}

	@Override
	public String getExchangeRatesHistoryAsCSV(Map<String, Object> params) {
		var formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		var rfc3339tail = "T00:00:00Z";
		var requestParams = new LinkedHashMap<String, String>();
		String resultString;

		// API key
		requestParams.put("key", this.key);

		// Start date (mandatory)
		var start = (LocalDate) params.get("start");
		requestParams.put("start", formatter.format(start).concat(rfc3339tail));
		
		// End date
		if (params.containsKey("end")) {
			var end = (LocalDate) params.get("end");
			requestParams.put("end", formatter.format(end).concat(rfc3339tail));
		}

		// Format
		requestParams.put("format", "csv");

		// Call endpoint, and return CSV string
		resultString = restClient.callEndpoint("exchange-rates/history", requestParams);
		return resultString;
	}

	@Override
	public List<ExchangeRateInterval> getExchangeRatesInterval(Map<String, Object> params) {
		var formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		var rfc3339tail = "T00:00:00Z";
		var collectionType = new TypeToken<List<ExchangeRateInterval>>(){}.getType();
		var requestParams = new LinkedHashMap<String, String>();
		List<ExchangeRateInterval> result;
		String resultString;

		// API key
		requestParams.put("key", this.key);

		// Start date (mandatory)
		var start = (LocalDate) params.get("start");
		requestParams.put("start", formatter.format(start).concat(rfc3339tail));
		
		// End date
		if (params.containsKey("end")) {
			var end = (LocalDate) params.get("end");
			requestParams.put("end", formatter.format(end).concat(rfc3339tail));
		}

		// Call endpoint, deserialize the JSON to a list of ExchangeRateInterval objects
		resultString = restClient.callEndpoint("exchange-rates/interval", requestParams);
		result = gson.fromJson(resultString, collectionType);
		return result;
	}

	@Override
	public String getExchangeRatesIntervalAsCSV(Map<String, Object> params) {
		var formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		var rfc3339tail = "T00:00:00Z";
		var requestParams = new LinkedHashMap<String, String>();
		String resultString;

		// API key
		requestParams.put("key", this.key);

		// Start date (mandatory)
		var start = (LocalDate) params.get("start");
		requestParams.put("start", formatter.format(start).concat(rfc3339tail));
		
		// End date
		if (params.containsKey("end")) {
			var end = (LocalDate) params.get("end");
			requestParams.put("end", formatter.format(end).concat(rfc3339tail));
		}

		// Format
		requestParams.put("format", "csv");

		// Call endpoint, and return CSV string
		resultString = restClient.callEndpoint("exchange-rates/interval", requestParams);
		return resultString;
	}

	/**
	 * Returns the used API key.
	 *
	 * @return API key
	 */
	public String getKey() {
		return key;
	}

	/**
	 * Sets the API to use.
	 *
	 * @param key API key
	 */
	public void setKey(String key) {
		this.key = key;
	}
}