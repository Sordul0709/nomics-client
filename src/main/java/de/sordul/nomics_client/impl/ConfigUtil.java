package de.sordul.nomics_client.impl;

import java.io.InputStream;
import java.util.Properties;

/**
 * Static utility class to access the clients configuration.
 */
public class ConfigUtil {
	private static final String propertyFileName = "configuration.properties";

	/**
	 * Returns either the property value for a given key or NULL if the key
	 * is undefined or something goes wrong.
	 *
	 * @param propertyName Key of the property to retrieve
	 * @return Value for the property key
	 */
	public static String getProp(String propertyName) {
		Properties properties;

		// Get properties from configuration file
		InputStream inputStream;
		try {
			inputStream = ConfigUtil.class.getClassLoader().getResourceAsStream(propertyFileName);
			properties = new Properties();
	
			if (inputStream != null) {
				properties.load(inputStream);
			}

			inputStream.close();
			return properties.getProperty(propertyName);

		} catch (Exception e) {
			System.out.println("Exception: " + e);
		}

		return null;
	}
}