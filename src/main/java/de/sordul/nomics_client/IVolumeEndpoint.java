package de.sordul.nomics_client;

import java.util.List;
import java.util.Map;

import de.sordul.nomics_client.model.Volume;

/**
 * Interface for all volume related endpoint-methods.
 */
public interface IVolumeEndpoint {

	/**
	 * Returns a list of Volume objects. A map of one or more of the following
	 * parameters is expected:
	 * - start : java.timne.LocalDate (mandatory)
	 * - end : java.timne.LocalDate
	 *
	 * @param params A map of parameter objects
	 * @return List of Volume objects
	 */
	List<Volume> getVolumeHistory(Map<String, Object> params);

	/**
	 * Returns a CSV as string. A map of one or more of the following
	 * parameters is expected:
	 * - start : java.timne.LocalDate (mandatory)
	 * - end : java.timne.LocalDate
	 *
	 * @param params A map of parameter objects
	 * @return CSV as string
	 */
	String getVolumeHistoryAsCSV(Map<String, Object> params);
}