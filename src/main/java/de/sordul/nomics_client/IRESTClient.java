package de.sordul.nomics_client;

import java.util.Map;

/**
 * Interface for REST calls. Instance of an implementing class is used by the
 * endpoint classes to do the communication via HTTP.
 */
public interface IRESTClient {

	/**
	 * Calls the desired Nomic endpoint with given request parameters and returns the response
	 * body as string.
	 *
	 * @param urlFragment Desired URL path (prefixed automatically by BASE_URL)
	 * @param urlParams Map of parameters to append
	 * @return The response body as string
	 */
	public String callEndpoint(String urlFragment, Map<String, String> urlParams);
}