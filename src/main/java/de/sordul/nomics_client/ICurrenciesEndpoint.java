package de.sordul.nomics_client;

import java.util.List;
import java.util.Map;

import de.sordul.nomics_client.model.MetaData;
import de.sordul.nomics_client.model.SparklineEntry;
import de.sordul.nomics_client.model.TickerData;

/**
 * Interface for all currency related endpoint-methods.
 */
public interface ICurrenciesEndpoint {

	/**
	 * Returns a list of TickerData objects. A map of zero or more of the following
	 * parameters is expected:
	 * - currencies : String[]
	 * - interval : de.sordul.nomics_client.model.Interval[]
	 * - convert : String
	 *
	 * @param params Map of request parameters
	 * @return List of TickerData objects
	 */
	List<TickerData> getTickerData(Map<String, Object> params);

	/**
	 * Returns a list of MetaData objects for requested currencies. A map of zero or more of the following
	 * parameters is expected:
	 * - currencies : String[]
	 * - attributes : de.sordul.nomics_client.model.MetaAttribute[]
	 *
	 * @param params A map of parameter objects
	 * @return List of MetaData objects
	 */
	List<MetaData> getMetaData(Map<String, Object> params);

	/**
	 * Returns a CSV String for requested currencies. A map of zero or more of the following
	 * parameters is expected:
	 * - currencies : String[]
	 * - attributes : de.sordul.nomics_client.model.MetaAttribute[]
	 *
	 * @param params A map of parameter objects
	 * @return CSV string
	 */
	String getMetaDataAsCSV(Map<String, Object> params);

	/**
	 * Returns prices for all currencies within a customizable time interval suitable for sparkline charts.
	 * A map of zero or more of the following parameters is expected:
	 * - start : java.time.LocalDate (mandatory)
	 * - end : java.time.LocalDate
	 *
	 * @param params A map of parameter objects
	 * @return List of Sparkline objects
	 */
	List<SparklineEntry> getCurrenciesSparkline(Map<String, Object> params);
}