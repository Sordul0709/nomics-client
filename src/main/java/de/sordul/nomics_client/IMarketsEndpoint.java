package de.sordul.nomics_client;

import java.util.List;
import java.util.Map;

import de.sordul.nomics_client.model.MarketCapHistory;
import de.sordul.nomics_client.model.MarketData;

/**
 * Interface for all market related endpoint-methods.
 */
public interface IMarketsEndpoint {

	/**
	 * Returns a list of MarketData objects. A map of zero or more of the following
	 * parameters is expected:
	 * - exchange : String
	 * - base : String[]
	 * - quote : String[]
	 *
	 * @param params A map of parameter objects
	 * @return List of MetaData objects
	 */
	List<MarketData> getMarketData(Map<String, Object> params);

	/**
	 * Returns a list of MarketData objects. A map of zero or more of the following
	 * parameters is expected:
	 * - exchange : String
	 * - base : String[]
	 * - quote : String[]
	 *
	 * @param params A map of parameter objects
	 * @return Meta-data as CSV string
	 */
	String getMarketDataAsCSV(Map<String, Object> params);

	/**
	 * Returns a list of MarketCapHistory objects. A map of zero or more of the following
	 * parameters is expected:
	 * - start : java.timne.LocalDate (mandatory)
	 * - end : java.timne.LocalDate
	 *
	 * @param params A map of parameter objects
	 * @return List of MarketCapHistory objects
	 */
	List<MarketCapHistory> getMarketCapHistory(Map<String, Object> params);

	/**
	 * Returns market capitalization data as CSV string. A map of zero or more of the following
	 * parameters is expected:
	 * - start : java.timne.LocalDate (mandatory)
	 * - end : java.timne.LocalDate
	 *
	 * @param params A map of parameter objects
	 * @return Market capitalization data as CSV string
	 */
	String getMarketCapHistoryAsCSV(Map<String, Object> params);
}