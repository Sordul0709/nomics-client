# Nomics API Client

A Java-based client for the Nomics Cryptocurrency & Bitcoin API v1.0.0. See https://nomics.com/ for reference. Only the standard (free) endpoints are supported currently.

## Getting Started

### Prerequisites

The client requires Java 12+ and Gradle to build.

### Installing

1. Check prerequisites
2. Clone the repository
3. Run `gradlew` (*nix / Mac) respectively `gradlew.bat` (Windows) in a terminal
4. A `build` folder should have bin created
5. The jar file is located in `build/libs`

## Running the tests

There is a set of unit- (*UnitTest.java) and integration-tests (IntegrationTest.java). The unit tests can be run without further prerequisites.

For the integration tests a `configuration.properties` file needs to be present in class-path (e.g. `src/test/resources`) containing the following line:

```
apiKey=YourNomicsAPIKey
```

## Usage

Get an instance of the client:

```java
var client = new NomicsClient("YourAPIKey");
```

The fields `currencies`, `markets`, `volume` and `rates` within the client instance contain methods for the corresponding API endpoints. Parameters are handed over as maps. `ImmutableMap` from Googles Guava library makes this especially simple.

Example of calling the `currencies` endpoint for ticker data:

```java
var requestParams = ImmutableMap.of(
	"currencies", new String[] {"BTC", "ETH", "XRP"}
);

List<TickerData> tickerData = client.currencies.getTickerData(requestParams);
```

See `NomicsClientIntegrationTest.java` for a basic overview and the other integration tests for further examples.


## Versioning

[SemVer](http://semver.org/) versioning is used.

## License

This project is licensed under the WTFPL License - see the [LICENSE](LICENSE) file for details.